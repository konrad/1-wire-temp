package main

import (
	"fmt"
	"io/ioutil"
	"regexp"
	"strconv"
	"strings"
)

var reg = regexp.MustCompile("t=\\d+")

func ParseAndSetTemp() {
	f, err := ioutil.ReadFile("/sys/bus/w1/devices/" + sensor + "/w1_slave")
	//f, err := ioutil.ReadFile("testsensorfile")
	if err != nil {
		fmt.Printf("error opening sensor file: %s\n", err)
		return
	}

	ct := parse(string(f))

	fmt.Printf("Setting current temperature to %f\n", ct)

	currentTemp.Set(ct)
}

func parse(w1Out string) float64 {
	t := reg.FindString(w1Out)
	if t == "" {
		return 0
	}

	temp := strings.TrimLeft(t, "t=")

	f, err := strconv.ParseFloat(temp, 64)
	if err != nil {
		fmt.Printf("error parsing temperature %s: %s\n", temp, err)
		return 0
	}

	return f / 1000
}
