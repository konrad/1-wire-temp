package main

import "testing"

func TestParse(t *testing.T) {
	t.Run("should find temperature", func(t *testing.T) {
		const out = `5b 01 4b 46 7f ff 0c 10 07 : crc=07 YES
5b 01 4b 46 7f ff 0c 10 07 t=21687`
		temp := parse(out)
		if temp != 21.687 {
			t.Errorf("temp is not 21.687, is %f", temp)
			t.Fail()
		}
	})
	t.Run("should not find temperature", func(t *testing.T) {
		const out = `5b 01 4b 46 7f ff 0c 10 07 : crc=07 YES
5b 01 4b 46 7f ff 0c 10 07`
		temp := parse(out)
		if temp != 0 {
			t.Errorf("temp is not 0, is %f", temp)
			t.Fail()
		}
	})
}
