package main

import (
	"flag"
	"fmt"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promauto"
	"net/http"
	"os"
	"time"

	"github.com/prometheus/client_golang/prometheus/promhttp"
)

var (
	sensor      string
	currentTemp = promauto.NewGauge(prometheus.GaugeOpts{
		Name: "current_temp",
		Help: "The current temperature",
	})
)

func init() {
	flag.StringVar(&sensor, "s", "", "The sensor id as it shows up in /sys/bus/w1/devices/.")
	flag.Parse()
}

func main() {
	if sensor == "" {
		fmt.Println("Please provide a sensor id via the -s flag.")
		os.Exit(1)
	}

	fmt.Printf("Using sensor %s\n", sensor)

	go func() {
		for {
			time.Sleep(5 * time.Second)
			ParseAndSetTemp()
		}
	}()

	fmt.Println("Starting http server on port 2112")

	http.Handle("/metrics", promhttp.Handler())
	http.ListenAndServe(":2112", nil)
}
