# 1-wire temperature metrics with prometheus

This is a simple package providing the temperature of a 1-wire sensor such as the DH1820 as prometheus metrics. It reads
the `w1_slave` file at `/sys/bus/w1/devices/<sensor>`, parses it and provides the read temperature at a webserver
running on port 2112 at `/metrics`.

## Usage

Simply run the compiled program and provide the id of the sensor with the `-s` flag. The id is the name of the folder at
`/sys/bus/w1/devices/` containing the `w1_slave` file.

## Building

To build this package for the raspberry pi zero:

```bash
GOOS=linux GOARCH=arm GOARM=6 go build -o 1-wire-temp-arm
```

To build for all other Pi's:

```bash
GOOS=linux GOARCH=arm GOARM=7 go build -o 1-wire-temp-arm
```

